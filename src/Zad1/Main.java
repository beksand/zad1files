package Zad1;


//Zadanie 1:
//        Należy napisać aplikację (wykorzystując załączony kod) która w pętli while czyta ze Scannera wejście
// użytkownika z konsoli, a następnie linia po linii wypisuje tekst do pliku. Aplikacja ma się zamykać po wpisaniu przez
// użytkownika komendy "quit".


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try (PrintWriter writer = new PrintWriter("file.txt")){
            while (sc.hasNextLine()){
            String line = sc.nextLine();
            if (line.equals("quit")){
                break;
            }

                writer.println(line);


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
